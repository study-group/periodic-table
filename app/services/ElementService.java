package services;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import dao.ElementDao;
import models.Element;
import play.Logger;

public class ElementService {

	@Inject
	private ElementDao dao;

	public List<Element> getElements() {
		Logger.info("[INITIALIZED - ElementService.getElements()].");
		try {
			return dao.getElements();
		} catch (SQLException e) {
			Logger.error("Exception in while ElementService.getElements()", e);
		}
		Logger.info("[FINISHED - ElementService.getElements()].");
		return null;
	}

	public List<String> getMetalGroups() {
		Logger.info("[INITIALIZED - ElementService.getMetalGroups()].");
		try {
			return dao.getMetalGroups();
		} catch (SQLException e) {
			Logger.error("Exception in while ElementService.getMetalGroups()", e);
		}
		Logger.info("[INITIALIZED - ElementService.getMetalGroups()].");
		return null;
	}

	public Element getElement(int atom) {
		Logger.info("[INITIALIZED - ElementService.getElement(atom:int)].");
		try {
			return dao.getElement(atom);
		} catch (SQLException e) {
			Logger.error("Exception in while ElementService.getElement(atom:int)", e);
		}
		Logger.info("[FINISHED - ElementService.getElement(atom:int)].");
		return null;
	}

	public Element getElement(String name) {
		Logger.info("[INITIALIZED - ElementService.getElement(name:String)].");
		try {
			return dao.getElementByName(name);
		} catch (SQLException e) {
			Logger.error("Exception in while ElementService.getElement(name:String)", e);
		}
		Logger.info("[FINISHED - ElementService.getElement(name:String)].");
		return null;
	}

	public List<Element> getElementsByMetalGroup(String metalGroup) {
		Logger.info("[INITIALIZED - ElementService.getElementsByMetalGroup(metalGroup:String)].");
		try {
			return dao.getElementsByMetalGroup(metalGroup);
		} catch (SQLException e) {
			Logger.error("Exception in while ElementService.getElementsByMetalGroup(metalGroup:String)", e);
		}
		Logger.info("[FINISHED - ElementService.getElementsByMetalGroup(metalGroup:String)].");
		return null;
	}

}
