package controllers;

import java.util.List;

import javax.inject.Inject;

import models.Element;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.ElementService;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class ElementController extends Controller {
	
	@Inject
	private ElementService service;

    public Result elements() {
    	Logger.info("[INITIALIZED - ElementController.elements()].");
    	List<Element> elements = service.getElements();
    	Logger.info("[FINISHED - ElementController.elements()].");
    	return ok(Json.toJson(elements));
    }
    
    public Result element(int atom) {
    	Logger.info("[INITIALIZED - ElementController.element(atom:int["+atom+"])].");
    	Element element = service.getElement(atom);
    	Logger.info("[FINISHED - ElementController.element(atom:int)].");
    	return ok(Json.toJson(element));
    }
    
    public Result elementByName(String name) {
    	Logger.info("[INITIALIZED - ElementController.elementByName(name:String["+name+"])].");
    	Element element = service.getElement(name);
    	Logger.info("[FINISHED - ElementController.elementByName(name:String)].");    	
    	return ok(Json.toJson(element));
    }
    
    public Result elementsByMetalGroup(String metalGroup) {
    	Logger.info("[INITIALIZED - ElementController.elementsByMetalGroup(metalGroup:String["+metalGroup+"])].");
    	List<Element> elements = service.getElementsByMetalGroup(metalGroup);
    	Logger.info("[FINISHED - ElementController.elementsByMetalGroup(metalGroup:String)].");
    	return ok(Json.toJson(elements));
    }
    
    public Result metalGroups() {
    	Logger.info("[INITIALIZED - ElementController.metalGroups()].");
    	List<String> elements = service.getMetalGroups();
    	Logger.info("[FINISHED - ElementController.metalGroups()].");
    	return ok(Json.toJson(elements));
    }

}
