package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import models.Element;
import play.Logger;
import play.db.Database;

public class ElementDao {

	private static final Element defaultElement = new Element("Search again", -1, "-", "-");;

	@Inject
	private Database db;

	public List<Element> getElements() throws SQLException {
		Logger.info("[INITIALIZED - ElementDao.getElements()].");
		
		List<Element> elements = new ArrayList<>();
				
		Connection conn = db.getConnection();
		Logger.info("Database Connection was successful.");

		String query = "Select * from PERIODIC_ELEMENTS ";
		PreparedStatement getElements = conn.prepareStatement(query);
		Logger.debug("Prepared Statement created!");
		
		ResultSet theElementsSet = getElements.executeQuery();
		Logger.debug("Query executed and dumped to ResultSet!");

		while (theElementsSet.next()) {
			elements.add(mapToElement(theElementsSet));
		}
		Logger.info("Total Elements fetched " + elements.size() + ".");

		getElements.close();
		conn.close();
		Logger.info("Database resources closed.");
		
		Logger.info("[FINISHED - ElementDao.getElements()].");
		return elements;
	}

	private Element mapToElement(ResultSet theElementSet) throws SQLException {
		Logger.debug("[INITIALIZED - ElementDao.mapToElement(theElementSet:ResultSet)].");
		
		String name = theElementSet.getString("ELEMENT");
		int atomicNumber = theElementSet.getInt("ATOMIC_NUMBER");
		String symbol = theElementSet.getString("SYMBOL");
		String metalGroup = theElementSet.getString("METAL_GROUP");
		Element element = new Element(name, atomicNumber, symbol, metalGroup);
		Logger.debug("Mapped Element - " + element);
		
		Logger.debug("[INITIALIZED - ElementDao.mapToElement(theElementSet:ResultSet)].");
		return element;
	}

	public List<String> getMetalGroups() throws SQLException {
		Logger.info("[INITIALIZED - ElementDao.getMetalGroups()].");
		
		String query = "Select DISTINCT METAL_GROUP from PERIODIC_ELEMENTS ";
		List<String> metalGroups = new ArrayList<>();
		
		Connection conn = db.getConnection();
		Logger.info("Database Connection was successful.");
		
		PreparedStatement getMetalGroups = conn.prepareStatement(query);
		Logger.debug("Prepared Statement created!");
		
		ResultSet theMetalGroupsSet = getMetalGroups.executeQuery();
		Logger.debug("Query executed and dumped to ResultSet!");

		while (theMetalGroupsSet.next()) {
			metalGroups.add(theMetalGroupsSet.getString(1));
		}
		Logger.info("Total Metal-Groups fetched " + metalGroups.size() + ".");

		getMetalGroups.close();
		conn.close();
		Logger.info("Database resources closed.");
		
		Logger.info("[FINISHED - ElementDao.getMetalGroups()].");
		return metalGroups;
	}

	public Element getElement(int number) throws SQLException {
		Logger.info("[INITIALIZED - ElementDao.getElement(number:int)].");
		
		String query = "Select * from PERIODIC_ELEMENTS where ATOMIC_NUMBER = ? ";
		
		Connection conn = db.getConnection();
		Logger.info("Database Connection was successful.");
		
		PreparedStatement getElements = conn.prepareStatement(query);
		Logger.debug("Prepared Statement created!");

		getElements.setInt(1, number);
		ResultSet theElementsSet = getElements.executeQuery();
		Logger.debug("Query executed with parameter-["+ number+"] and dumped to ResultSet!");

		Element element = (theElementsSet.next() ? mapToElement(theElementsSet) : defaultElement);
		Logger.info("Element fetched " + element + ".");

		getElements.close();
		conn.close();
		Logger.info("Database resources closed.");
		
		Logger.info("[FINISHED - ElementDao.getElement(number:int)].");
		return element;
	}

	public Element getElementByName(String elementName) throws SQLException {
		Logger.info("[INITIALIZED - ElementDao.getElementByName(elementName:String)].");
		String query = "Select * from PERIODIC_ELEMENTS where ELEMENT = ? ";
		
		Connection conn = db.getConnection();
		Logger.info("Database Connection was successful.");
		
        PreparedStatement getElements = conn.prepareStatement(query);
        Logger.debug("Prepared Statement created!");
        
        getElements.setString(1, elementName);
        ResultSet theElementsSet = getElements.executeQuery();
        Logger.debug("Query executed with parameter-["+ elementName+"] and dumped to ResultSet!");
        
        Element element = (theElementsSet.next()?mapToElement(theElementsSet):defaultElement);
        Logger.info("Element fetched " + element + ".");
        
        getElements.close();
        conn.close();
        Logger.info("Database resources closed.");
        
        Logger.info("[FINISHED - ElementDao.getElementByName(elementName:String)].");
		return element;
	}

	public List<Element> getElementsByMetalGroup(String metalGroup) throws SQLException {
		Logger.info("[INITIALIZED - ElementDao.getElementsByMetalGroup(metalGroup:String)].");
		
		List<Element> elements = new ArrayList<>();
		String query = "Select * from PERIODIC_ELEMENTS where METAL_GROUP = ?";
		
		Connection conn = db.getConnection();
		Logger.info("Database Connection was successful.");
		
		PreparedStatement getElements = conn.prepareStatement(query);
		Logger.debug("Prepared Statement created!");
		
		getElements.setString(1, metalGroup);
		ResultSet theElementsSet = getElements.executeQuery();
		Logger.debug("Query executed with parameter-["+ metalGroup+"] and dumped to ResultSet!");

		while (theElementsSet.next()) {
			elements.add(mapToElement(theElementsSet));
		}
		Logger.info("Total Elements fetched " + elements.size() + ".");

		getElements.close();
		conn.close();
		Logger.info("Database resources closed.");
		
		Logger.info("[FINISHED - ElementDao.getElementsByMetalGroup(metalGroup:String)].");
		return elements;
	}

}
