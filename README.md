# README #

This is a REST API built on Java Play framework.

It gives responses in JSON format, for the elements of periodic table.

### What is this repository for? ###

* Responds elements of periodic table in JSON format.
* Version - 1
* Plans to expand it in such a way that, when two elements are mixed, it will respond with it's composition.

### How do I get set up? ###

* Required Activator (https://www.playframework.com/download)
* Required JDK.
* Dependencies are mentioned in build.sbt
* Play embedded H2 database used currently.
* Deployment instructions - in the root of project - activator run.
* Unit Tests need to be written yet.

It will run on localhost:9000 port.

### Who do I talk to? ###

* Repo owner - Hiren Parmar (hiren_parmar@outlook.com)

### This API is live on Heroku  

(https://periodtable.herokuapp.com) 


* To access all elements - https://periodtable.herokuapp.com/elements
* To access 1 element - https://periodtable.herokuapp.com/element/{atomic-number}
* * eg: https://periodtable.herokuapp.com/element/1 - Helium
* * eg: https://periodtable.herokuapp.com/element/6 - Carbon
* To access metal groups - https://periodtable.herokuapp.com/metal-groups
* To access elements specific to a metal group - https://periodtable.herokuapp.com/elements/{metal-group}
* * eg: https://periodtable.herokuapp.com/elements/diatomicnonmetal